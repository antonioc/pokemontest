package com.wultimaproject.pokemontest.presentation.list

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.wultimaproject.pokemontest.common.RxTestSchedulerRule
import com.wultimaproject.pokemontest.model.PokemonCompat
import com.wultimaproject.pokemontest.repository.pokemonListRepository
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.test.KoinTestRule
import org.mockito.Mockito
import org.mockito.Mockito.*
import java.util.concurrent.Callable


class PokemonListViewModelTest() {
    @get:Rule
    val koinRule = KoinTestRule.create {
        modules(pokemonListRepository)
    }

    @get:Rule
    val rule = RxTestSchedulerRule()

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()


    private lateinit var sut: PokemonListViewModel




        @Test
        fun dispatchLoadActionTest() {
            sut = PokemonListViewModel(PokemonListState(isIdle = true))

            val stateLoading = PokemonListState(
                pokemons = listOf(),
                isLoading = true,
                isError = false,
                isIdle = false,
                total = 0,
                page = 0
            )

            sut.dispatch(PokemonListAction.LoadPokemonsList(true))
            sut.observablePokemonState.observeForever {}
            assertEquals(sut.observablePokemonState.value, stateLoading)

        }

}

