package com.wultimaproject.pokemontest.repository

import com.wultimaproject.pokemontest.di.networkModule
import com.wultimaproject.pokemontest.model.*
import io.reactivex.observers.TestObserver
import junit.framework.Assert.assertEquals
import junit.framework.Assert.assertNotSame
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.koin.test.KoinTestRule


class PokemonListRepositoryTest {
    @get:Rule
    val koinRule = KoinTestRule.create {
        modules(networkModule)
    }

    private lateinit var sut: PokemonListRepository


    @Before
    fun setup() {
        sut = PokemonListRepository()
    }

    @Test
    fun getPokemonList_success_returnList() {
        val response = PokemonCompatResponse(10, getPokemonCompatList())
        val testObserver: TestObserver<PokemonCompatResponse> =
            sut.getPokemonListObservable(5).test()
        testObserver.onNext(response)
        testObserver.assertValues(response)
        assertEquals(response.results, getPokemonCompatList())
    }

    @Test
    fun getPokemonList_failure_noList() {
        val response = PokemonCompatResponse(0, listOf())
        val testObserver: TestObserver<PokemonCompatResponse> =
            sut.getPokemonListObservable(5).test()
        testObserver.onNext(response)
        testObserver.assertValues(response)
        assertNotSame(response.results, getPokemonCompatList())
    }

    @Test
    fun getPokemonDetails_success_returnDetails() {
        val name = "realPokemonName"
        val response = getPokemonDetailsResponse(name)
        val testObserver: TestObserver<PokemonDetailsResponse> =
            sut.getPokemonDetailsObservable(name).test()
        testObserver.onNext(response)
        testObserver.assertValues(response)
        assertEquals(response, getPokemonDetailsResponse(name))
    }

    @Test
    fun getPokemonDetails_failure_noDetails() {
        val name = "realPokemonName"
        val fakeName = "fakeName"
        val response = getPokemonDetailsResponse(fakeName)
        val testObserver: TestObserver<PokemonDetailsResponse> =
            sut.getPokemonDetailsObservable(name).test()
        testObserver.onNext(response)
        testObserver.assertValues(response)
        assertNotSame(response, getPokemonDetailsResponse(name))
    }


    private fun getPokemonCompatList(): List<PokemonCompat> {
        return listOf(PokemonCompat("name1", "url1"), PokemonCompat("name2", "url2"))
    }

    private fun getPokemonDetailsResponse(name: String): PokemonDetailsResponse {
        val stats = ArrayList<GroupStatsSchemas>()
        val types = ArrayList<GroupTypes>()
        return PokemonDetailsResponse(name, Sprites("front_shiny"), stats, types)
    }


}

