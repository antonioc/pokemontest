package com.wultimaproject.pokemontest.common

import android.view.WindowManager
import android.widget.ImageView
import android.widget.Toast
import androidx.core.widget.ContentLoadingProgressBar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import com.bumptech.glide.Glide

fun ContentLoadingProgressBar.showAndLockScreen(activity: FragmentActivity) {
    activity.window.setFlags(
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

    this.show()
}

fun ContentLoadingProgressBar.hideAndUnlockScreen(activity: FragmentActivity) {
    activity.window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)

    this.hide()
}


fun Fragment.showToast(message: String){
    Toast.makeText(requireContext(),message, Toast.LENGTH_LONG).show()
}

fun ImageView.setGlideImage(urlImage: String){
    Glide.with(this).load(urlImage).into(this)

}