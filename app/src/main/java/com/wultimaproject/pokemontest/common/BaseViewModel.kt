package com.wultimaproject.pokemontest.common

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.wultimaproject.pokemontest.presentation.list.PokemonListState
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.subjects.PublishSubject

open class BaseViewModel<A, S> : ViewModel() {
    protected val disposables: CompositeDisposable = CompositeDisposable()
    protected val actions: PublishSubject<A> = PublishSubject.create<A>()
    protected val state = MutableLiveData<S>()

    val observablePokemonState: LiveData<S> = MediatorLiveData<S>().apply {
        addSource(state) { data ->
            setValue(data)
        }
    }
}