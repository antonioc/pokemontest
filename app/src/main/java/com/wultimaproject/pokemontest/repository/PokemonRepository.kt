package com.wultimaproject.pokemontest.repository

import com.wultimaproject.pokemontest.model.PokemonCompatResponse
import com.wultimaproject.pokemontest.model.PokemonDetailsResponse
import com.wultimaproject.pokemontest.network.PokemonNetworkInterface
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import org.koin.dsl.module
import org.koin.java.KoinJavaComponent.inject

val pokemonListRepository = module {
    single { PokemonListRepository() }
}


class PokemonListRepository {
    private val pokemonNetworkInterface: PokemonNetworkInterface by inject(PokemonNetworkInterface::class.java)

    fun getPokemonListObservable(offset: Int): Observable<PokemonCompatResponse> {
        return Observable.create { emitter ->
            pokemonNetworkInterface.loadPokemonList(offset)
                .observeOn(Schedulers.io())
                .subscribe({ pokemons ->
                    emitter.onNext(pokemons)
                }, {
                    emitter.onError(Throwable())
                })
        }
    }


    fun getPokemonDetailsObservable(url: String): Observable<PokemonDetailsResponse> {
        return Observable.create { emitter ->
            pokemonNetworkInterface.loadPokemonDetails(url)
                .observeOn(Schedulers.io())
                .subscribe({ pokemons ->
                    emitter.onNext(pokemons)
                }, {
                    emitter.onError(Throwable())
                })
        }
    }
}