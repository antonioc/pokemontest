package com.wultimaproject.pokemontest.di

import com.squareup.moshi.Moshi
import com.wultimaproject.pokemontest.BuildConfig
import com.wultimaproject.pokemontest.network.NetworkClient
import com.wultimaproject.pokemontest.network.PokemonNetworkInterface
import com.wultimaproject.pokemontest.network.PokemonNetworkInterfaceImpl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {
    factory { provideMoshi() }
    single { provideRetrofit(get()) }
    single { provideApiClient(get())}
    single { provideNetworkClientInterface() }
}


fun provideMoshi(): MoshiConverterFactory {
    val moshi = Moshi.Builder().build()
    return MoshiConverterFactory.create(moshi)
}

fun provideRetrofit(
    moshiConverterFactory: MoshiConverterFactory
): Retrofit {
    val builder = OkHttpClient.Builder()

    if (BuildConfig.DEBUG) {
        val loggingInterceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
        builder.addInterceptor(loggingInterceptor)
    }

    val client = builder
        .connectTimeout(5, TimeUnit.SECONDS)
        .readTimeout(5L, TimeUnit.SECONDS)
        .writeTimeout(5L, TimeUnit.SECONDS)
        .build()

    return Retrofit.Builder()
        .baseUrl("https://pokeapi.co/api/v2/")
        .client(client)
        .addConverterFactory(moshiConverterFactory)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

fun provideApiClient(retrofit: Retrofit): NetworkClient = retrofit.create(
    NetworkClient::class.java
)

fun provideNetworkClientInterface(): PokemonNetworkInterface {
    return PokemonNetworkInterfaceImpl()
}






