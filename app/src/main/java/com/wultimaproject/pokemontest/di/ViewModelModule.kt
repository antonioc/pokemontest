package com.wultimaproject.pokemontest.di

import com.wultimaproject.pokemontest.presentation.detail.PokemonDetailsState
import com.wultimaproject.pokemontest.presentation.detail.PokemonDetailsViewModel
import com.wultimaproject.pokemontest.presentation.list.PokemonListViewModel
import com.wultimaproject.pokemontest.presentation.list.PokemonListState
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val viewModelModule = module {
    viewModel { PokemonListViewModel(PokemonListState(isIdle = true)) }
    viewModel { PokemonDetailsViewModel(PokemonDetailsState(isIdle = true)) }
}
