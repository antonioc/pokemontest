package com.wultimaproject.pokemontest.presentation.list

import com.wultimaproject.pokemontest.common.BaseViewModel
import com.wultimaproject.pokemontest.common.Reducer
import com.wultimaproject.pokemontest.presentation.list.PokemonListChange.Companion.GENERIC_ERROR_CODE
import com.wultimaproject.pokemontest.repository.PokemonListRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import org.koin.java.KoinJavaComponent.inject
import timber.log.Timber
import java.util.*

class PokemonListViewModel(initialPokemonListState: PokemonListState?) :
    BaseViewModel<PokemonListAction, PokemonListState>() {

    private val listRepository by inject(PokemonListRepository::class.java)

    private val initialState = initialPokemonListState ?: PokemonListState(isIdle = true)
    var offset: Int = -1
    var totalCount: Int = 0

    private val reducer: Reducer<PokemonListState, PokemonListChange> = { state, change ->
        when (change) {
            is PokemonListChange.Loading -> {
                state.copy(
                    isIdle = false,
                    isLoading = true,
                    pokemons = Collections.emptyList(),
                    isError = false
                )
            }
            is PokemonListChange.Error -> {
                state.copy(
                    isLoading = false,
                    isError = true,
                    isIdle = false,
                    pokemons = listOf()
                )
            }
            is PokemonListChange.Loaded -> {
                state.copy(
                    pokemons = change.listPokemon,
                    isLoading = false,
                    isError = false,
                    isIdle = false,
                    total = (totalCount - offset) / 20,
                    page = offset / 20 + 1
                )
            }
        }
    }

    init {
        bindAction()
        dispatch(PokemonListAction.LoadPokemonsList(true))
    }

    fun dispatch(pokemonListAction: PokemonListAction) {
        if (pokemonListAction is PokemonListAction.LoadPokemonsList) {
            if (pokemonListAction.forward) {
                when {
                    offset < 0 -> {
                        offset = 0
                    }
                    offset == 0 -> {
                        offset = 20
                    }
                    else -> {
                        offset += 20
                    }
                }
            } else {
                if (offset > 0)
                    offset -= 20
            }
            actions.onNext(pokemonListAction)
        }
    }


    private fun bindAction() {
        val loadActionChange = actions.ofType<PokemonListAction.LoadPokemonsList>()
            .switchMap {
                listRepository.getPokemonListObservable(offset)
                    .subscribeOn(Schedulers.io())
                    .map<PokemonListChange> { responseListPokemon ->
                        totalCount.takeIf { it == 0 }?.let {
                            totalCount = responseListPokemon.count
                        }
                        PokemonListChange.Loaded(responseListPokemon.results)
                    }
                    .defaultIfEmpty(PokemonListChange.Loaded(listOf()))
                    .onErrorReturn { PokemonListChange.Error(GENERIC_ERROR_CODE) }
                    .startWith(PokemonListChange.Loading)
            }

        disposables += loadActionChange
            .scan(initialState, reducer)
            .distinctUntilChanged()
            .filter { !it.isIdle }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { newState ->
                    state.value = newState
                },
                { Timber.d("error") })
    }


}