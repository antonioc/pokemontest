package com.wultimaproject.pokemontest.presentation.detail

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.wultimaproject.pokemontest.R
import com.wultimaproject.pokemontest.model.Stats
import kotlinx.android.synthetic.main.custom_stats_bars_container.view.*


class StatsBarsContainer(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {
    init {
        inflate(context, R.layout.custom_stats_bars_container, this)
    }

    fun setStats(statsToShow: List<Stats>) {
        statsToShow.forEachIndexed { index, stats ->
            custom_stats_bar_container.addView(addStatBar(stats, index))
        }
        custom_stats_bar_container.requestLayout()
    }

    private fun addStatBar(stat: Stats, position: Int): View {
        val statBar = StatsBar(context)
        statBar.setStat(stat, position)
        return statBar
    }
}