package com.wultimaproject.pokemontest.presentation.list

import com.wultimaproject.pokemontest.model.PokemonCompat

data class PokemonListState(
    val pokemons: List<PokemonCompat> = listOf(),
    val isIdle: Boolean = false,
    val isLoading: Boolean = false,
    val isError: Boolean = false,
    val total:Int = 0,
    val page: Int = 0
)