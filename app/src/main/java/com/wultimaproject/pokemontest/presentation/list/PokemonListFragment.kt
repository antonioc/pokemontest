package com.wultimaproject.pokemontest.presentation.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import com.wultimaproject.pokemontest.R
import com.wultimaproject.pokemontest.common.hideAndUnlockScreen
import com.wultimaproject.pokemontest.common.showAndLockScreen
import com.wultimaproject.pokemontest.common.showToast
import com.wultimaproject.pokemontest.model.PokemonCompat
import com.wultimaproject.pokemontest.presentation.detail.PokemonDetailsFragment.Companion.FRAGMENT_DETAILS_TITLE
import com.wultimaproject.pokemontest.presentation.detail.PokemonDetailsFragment.Companion.NAME_POKEMON_DETAIL
import com.wultimaproject.pokemontest.presentation.detail.PokemonDetailsFragment.Companion.URL_POKEMON_DETAILS
import kotlinx.android.synthetic.main.error_message.*
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.progress_bar.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class PokemonListFragment : Fragment(), PokemonListAdapter.OnPokemonSelectedInterface {

    private val viewModel by viewModel<PokemonListViewModel>()
    private val adapter by lazy { PokemonListAdapter(this) }

    private val animController by lazy {
        AnimationUtils.loadLayoutAnimation(
            context,
            R.anim.anim_fall_down
        )
    }


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        list_pagination_back.setOnClickListener {
            viewModel.dispatch(PokemonListAction.LoadPokemonsList(false))
        }

        list_pagination_next.setOnClickListener {
            viewModel.dispatch(PokemonListAction.LoadPokemonsList(true))
        }

        error_message_btn.setOnClickListener {
            viewModel.dispatch(PokemonListAction.LoadPokemonsList(false))
        }

        viewModel.observablePokemonState.observe(viewLifecycleOwner, Observer {
            it?.let { renderState(it) }
        })
    }

    private fun renderState(pokemonListState: PokemonListState) {
        with(pokemonListState) {
            when {
                isIdle -> {
                }
                pokemons.isNotEmpty() -> {
                    list_rv.visibility = View.VISIBLE
                    list_pagination_controls.visibility = View.VISIBLE
                    error_message_container.visibility = View.GONE
                    progress_bar.hideAndUnlockScreen(requireActivity())

                    showList(pokemons)
                    setPagination(page, total)
                }
                isLoading -> {
                    list_rv.visibility = View.GONE
                    list_pagination_controls.visibility = View.GONE
                    error_message_container.visibility = View.GONE
                    progress_bar.showAndLockScreen(requireActivity())
                }
                isError -> {
                    list_rv.visibility = View.GONE
                    list_pagination_controls.visibility = View.GONE
                    error_message_container.visibility = View.VISIBLE
                    progress_bar.hideAndUnlockScreen(requireActivity())

                    showToast(resources.getString(R.string.something_wrong))
                }
            }
        }
    }

    private fun showList(list: List<PokemonCompat>) {
        list_rv.adapter = adapter
        list_rv.layoutAnimation = animController
        adapter.setPokemonList(list)
        list_rv.scheduleLayoutAnimation()


    }

    private fun setPagination(page: Int, total: Int) {
        list_pagination_text.text = resources.getString(R.string.pagination_text, page, total)
    }

    override fun onPokemonSelected(pokemon: PokemonCompat, titleHeaderView: View) {
        val transAnim = FragmentNavigatorExtras(
            titleHeaderView to resources.getString(R.string.transition_title).plus(pokemon.name)
        )

        findNavController().navigate(
            R.id.action_pokemonListFragment_to_pokemonDetailsFragment,
            Bundle().apply {
                putString(URL_POKEMON_DETAILS, pokemon.url)
                putString(NAME_POKEMON_DETAIL, pokemon.name.capitalize())
                putString(
                    FRAGMENT_DETAILS_TITLE,
                    resources.getString(R.string.fragment_details_title)
                )
            }, null, transAnim
        )
    }

}