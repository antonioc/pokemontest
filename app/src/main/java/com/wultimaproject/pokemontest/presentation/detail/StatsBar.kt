package com.wultimaproject.pokemontest.presentation.detail

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import com.wultimaproject.pokemontest.R
import com.wultimaproject.pokemontest.model.Stats
import kotlinx.android.synthetic.main.custom_stats_bar.view.*


class StatsBar : LinearLayout {
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context) : super(context) {
        inflate(context, R.layout.custom_stats_bar, this)
    }

    fun setStat(stat: Stats, position: Int){
        stats_bar_title.text = resources.getString(R.string.stats_bar_title, styleStasName(stat.baseStateName), stat.baseStateValue)
        stats_bar_progress.setProgress(stat.baseStateValue, true)
        if (stat.baseStateValue > 100 ){
            stats_bar_super_effective.visibility = View.VISIBLE
            stats_bar_progress_plus.visibility = View.VISIBLE
        }
    }

    private fun styleStasName(statsTobeStyled : String): String{
        return statsTobeStyled.replace("-", " ").capitalize()
    }



}