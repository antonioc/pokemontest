package com.wultimaproject.pokemontest.presentation.list

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.wultimaproject.pokemontest.R
import com.wultimaproject.pokemontest.model.PokemonCompat
import kotlinx.android.synthetic.main.item_pokemon.view.*

class PokemonListAdapter(val callback: OnPokemonSelectedInterface) :
    RecyclerView.Adapter<PokemonListViewHolder>() {

    interface OnPokemonSelectedInterface {
        fun onPokemonSelected(pokemon: PokemonCompat, itemView: View)
    }

    lateinit var pokemons: List<PokemonCompat>

    lateinit var context: Context


    fun setPokemonList(list: List<PokemonCompat>) {
        pokemons = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokemonListViewHolder {

        context = parent.context
        val inflater = LayoutInflater.from(context)
        return PokemonListViewHolder(inflater.inflate(R.layout.item_pokemon, parent, false))
    }

    override fun getItemCount(): Int {
        return pokemons.size
    }

    override fun onBindViewHolder(holder: PokemonListViewHolder, position: Int) {
        if (position % 2 == 0) {
            holder.itemView.setBackgroundColor(context.resources.getColor(R.color.white, null))
            holder.itemView.item_pokemon_id.setTextColor(
                context.resources.getColor(
                    R.color.colorYellow,
                    null
                )
            )
        } else {
            holder.itemView.setBackgroundColor(
                context.resources.getColor(
                    R.color.colorYellow,
                    null
                )
            )
            holder.itemView.item_pokemon_id.setTextColor(
                context.resources.getColor(
                    R.color.white,
                    null
                )
            )
        }
        holder.bind(pokemons[position])
        holder.itemView.item_pokemon_text.transitionName =
            context.resources.getString(R.string.transition_title).plus(pokemons[position].name)
        holder.itemView.setOnClickListener {
            callback.onPokemonSelected(
                pokemons[position],
                holder.itemView.item_pokemon_text
            )
        }
    }
}


class PokemonListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(pokemon: PokemonCompat) {
        itemView.item_pokemon_id.text = getPokemonNumber(pokemon.url)
        itemView.item_pokemon_text.text = formatPokemonName(pokemon.name).capitalize()
    }

    private fun getPokemonNumber(url: String): String {
        val re = Regex("[A-Za-z/]")
        return url.substring(url.indexOf("pokemon"), url.length - 1).replace(re, "")
    }

    private fun formatPokemonName(pokemonName: String): String {
        return pokemonName.replace(
            ("\\s*-incarnate\\s*" + "|\\s*-normal\\s*|\\s*-ordinary\\s*|\\s*-altered\\s*|\\s*-standard\\s|\\s*-shield\\s*|\\s*-solo\\s*").toRegex(),
            ""
        )
    }
}