package com.wultimaproject.pokemontest.presentation.list

import com.wultimaproject.pokemontest.model.PokemonCompat

sealed class PokemonListChange {
    object Loading : PokemonListChange()
    data class Loaded(val listPokemon: List<PokemonCompat>) : PokemonListChange()
    data class Error(var code: Int) : PokemonListChange()


    companion object{
        const val GENERIC_ERROR_CODE = 49
    }
}






