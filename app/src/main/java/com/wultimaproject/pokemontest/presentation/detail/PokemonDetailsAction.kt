package com.wultimaproject.pokemontest.presentation.detail

import java.util.*

sealed class PokemonDetailsAction {
    data class LoadPokemonDetails(val url:String): PokemonDetailsAction()
}