package com.wultimaproject.pokemontest.presentation.detail

import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.animation.BounceInterpolator
import android.view.animation.LinearInterpolator
import android.view.animation.OvershootInterpolator
import androidx.fragment.app.Fragment
import androidx.transition.TransitionInflater
import com.google.android.material.chip.Chip
import com.wultimaproject.pokemontest.R
import com.wultimaproject.pokemontest.common.hideAndUnlockScreen
import com.wultimaproject.pokemontest.common.setGlideImage
import com.wultimaproject.pokemontest.common.showAndLockScreen
import com.wultimaproject.pokemontest.common.showToast
import com.wultimaproject.pokemontest.model.PokemonDetails
import kotlinx.android.synthetic.main.details_header.*
import kotlinx.android.synthetic.main.error_message.*
import kotlinx.android.synthetic.main.fragment_details.*
import kotlinx.android.synthetic.main.progress_bar.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class PokemonDetailsFragment : Fragment() {
    private val viewModel by viewModel<PokemonDetailsViewModel>()

    private val url by lazy { arguments?.getString(URL_POKEMON_DETAILS) }
    private val name by lazy { arguments?.getString(NAME_POKEMON_DETAIL) }

    private lateinit var anim: AnimatorSet

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val transition =
            TransitionInflater.from(context).inflateTransition(android.R.transition.move)

        sharedElementEnterTransition = transition
        sharedElementReturnTransition = transition
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        details_title.text = name
        scheduleStartPostponedTransition()

        url?.let { viewModel.dispatch(PokemonDetailsAction.LoadPokemonDetails(it)) }
        viewModel.observablePokemonState.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
            it?.let { renderState(it) }
        })

        error_message_btn.setOnClickListener {
            url?.let { viewModel.dispatch(PokemonDetailsAction.LoadPokemonDetails(it)) }
        }

        setupPokeBallAnim()

        details_title.apply {
            transitionName = resources.getString(R.string.transition_title).plus(name).toLowerCase()
        }

    }

    private fun renderState(pokemonDetails: PokemonDetailsState) {
        with(pokemonDetails) {
            when {
                isIdle -> {
                }
                pokemon != null -> {
                    error_message_container.visibility = View.GONE
                    showPokemondDetails(pokemon)
                    progress_bar.hideAndUnlockScreen(requireActivity())
                }
                isLoading -> {
                    progress_bar.showAndLockScreen(requireActivity())
                }
                isError -> {
                    progress_bar.hideAndUnlockScreen(requireActivity())
                    error_message_container.visibility = View.VISIBLE
                    showToast(resources.getString(R.string.something_wrong))
                }
            }
        }
    }

    private fun showPokemondDetails(pokemon: PokemonDetails) {
        detail_header.visibility = View.VISIBLE
        details_stats_container.visibility = View.VISIBLE
        detail_type_title.visibility = View.VISIBLE
        detail_sprite.setGlideImage(pokemon.sprites)
        setChipsForTypes(pokemon.types)
        details_stats_container.setStats(pokemon.stats)
        animatePokeBall()
    }

    private fun setChipsForTypes(types: List<String>) {
        types.forEach {
            val chip = Chip(context)
            chip.text = it.capitalize()
            details_type_chipgroup.addView(chip)
        }
    }

    private fun setupPokeBallAnim() {

        val y: Float = detail_pokeball.translationY
        val bounceDistance = 50f
        val exitDistance = 1000f

        val wiggleAnim =
            ObjectAnimator.ofFloat(detail_pokeball, "rotation", -10f, 10f, -8f, 12f, -12f, 10f, 0f)
                .apply {
                    duration = 1000
                    interpolator = OvershootInterpolator()
                }

        val bounceAnim =
            ObjectAnimator.ofFloat(
                detail_pokeball,
                "translationY",
                y,
                y - bounceDistance,
                y,
                y - bounceDistance,
                y
            )
                .apply {
                    duration = 500
                    interpolator = BounceInterpolator()
                }

        val rollAnim =
            ObjectAnimator.ofFloat(detail_pokeball, "rotation", 0f, 360f)
                .apply {
                    duration = 500
                    interpolator = LinearInterpolator()
                }

        val exitAnim =
            ObjectAnimator.ofFloat(detail_pokeball, "translationX", y, y + exitDistance)
                .apply {
                    duration = 500
                    interpolator = LinearInterpolator()
                }

        anim = AnimatorSet().apply {
            play(wiggleAnim).before(bounceAnim)
            play(bounceAnim).after(wiggleAnim)
            play(rollAnim).with(exitAnim).after(bounceAnim)
        }
    }

    private fun animatePokeBall() {
        AnimatorSet().apply {
            play(anim)
            start()
        }
    }


    private fun scheduleStartPostponedTransition() {
        details_title.viewTreeObserver.addOnPreDrawListener(
            object : ViewTreeObserver.OnPreDrawListener {
                override fun onPreDraw(): Boolean {
                    details_title.viewTreeObserver.removeOnPreDrawListener(this)
                    startPostponedEnterTransition()
                    return true
                }
            })
    }


    companion object {
        const val URL_POKEMON_DETAILS = "url_pokemon_details"
        const val NAME_POKEMON_DETAIL = "name_pokemon_detail"
        const val FRAGMENT_DETAILS_TITLE = "fragment_details_title"
    }
}