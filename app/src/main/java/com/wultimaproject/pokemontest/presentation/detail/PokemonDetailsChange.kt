package com.wultimaproject.pokemontest.presentation.detail

import com.wultimaproject.pokemontest.model.PokemonDetails
import com.wultimaproject.pokemontest.model.PokemonDetailsResponse

sealed class PokemonDetailsChange {
    object Loading : PokemonDetailsChange()
    data class Loaded(val details: PokemonDetails?) : PokemonDetailsChange()
    data class Error(var code: Int) : PokemonDetailsChange()
}