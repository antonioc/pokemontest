package com.wultimaproject.pokemontest.presentation.detail

import com.wultimaproject.pokemontest.common.BaseViewModel
import com.wultimaproject.pokemontest.common.Reducer
import com.wultimaproject.pokemontest.model.GroupStatsSchemas
import com.wultimaproject.pokemontest.model.GroupTypes
import com.wultimaproject.pokemontest.model.PokemonDetails
import com.wultimaproject.pokemontest.model.Stats
import com.wultimaproject.pokemontest.presentation.list.PokemonListChange.Companion.GENERIC_ERROR_CODE
import com.wultimaproject.pokemontest.repository.PokemonListRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.schedulers.Schedulers
import org.koin.java.KoinJavaComponent.inject
import timber.log.Timber


class PokemonDetailsViewModel(initialPokemonDetailsState: PokemonDetailsState?) : BaseViewModel<PokemonDetailsAction, PokemonDetailsState>(){

    private val detailsRepository by inject(PokemonListRepository::class.java)

    private val initialState = initialPokemonDetailsState ?: PokemonDetailsState(isIdle = true)

    private val reducer: Reducer<PokemonDetailsState, PokemonDetailsChange> = { state, change ->
        when (change) {
            is PokemonDetailsChange.Loading -> {
                state.copy(
                    isIdle = false,
                    isLoading = true,
                    pokemon = null,
                    isError = false
                )
            }
            is PokemonDetailsChange.Error -> {
                state.copy(
                    isIdle = false,
                    isLoading = false,
                    pokemon = null,
                    isError = true
                )
            }
            is PokemonDetailsChange.Loaded -> {
                state.copy(
                    isIdle = false,
                    isLoading = false,
                    pokemon = change.details,
                    isError = false
                )
            }
        }
    }

     init {
        bindAction()
    }

    fun dispatch(pokemonDetailsAction: PokemonDetailsAction){
        actions.onNext(pokemonDetailsAction)
    }

    private fun bindAction(){
        val loadPokemonDetailsAction = actions.ofType<PokemonDetailsAction.LoadPokemonDetails>()
            .switchMap {
                detailsRepository.getPokemonDetailsObservable(it.url)
                    .subscribeOn(Schedulers.io())

                    .map<PokemonDetailsChange>{response ->
                         PokemonDetailsChange.Loaded(
                             PokemonDetails(
                                 response.name,
                                 response.sprites.front_shiny,
                                 getStatsList(response.stats),
                                 getTypesList(response.types)
                             )
                         )

                    }
                    .defaultIfEmpty(PokemonDetailsChange.Loaded(null))
                    .onErrorReturn { PokemonDetailsChange.Error(GENERIC_ERROR_CODE) }
                    .startWith(PokemonDetailsChange.Loading)
            }

        disposables += loadPokemonDetailsAction
            .scan(initialState,reducer)
            .distinctUntilChanged()
            .filter { !it.isIdle }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { newState ->
                    state.value = newState
                },
                { Timber.d("error") })

    }

    private fun getStatsList(listGroupStats: List<GroupStatsSchemas>): List<Stats>{
        val listToReturn = mutableListOf<Stats>()
        listGroupStats.forEach {
            listToReturn.add( Stats(baseStateName = it.stat.name, baseStateValue = it.base_stat))
        }
        return listToReturn
    }

    private fun getTypesList(listGroupType: List<GroupTypes>) : List<String>{
        val listToReturn = mutableListOf<String>()
        listGroupType.forEach {
            listToReturn.add(it.type.name)
        }
        return listToReturn
    }


}