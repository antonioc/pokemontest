package com.wultimaproject.pokemontest.presentation.detail

import com.wultimaproject.pokemontest.model.PokemonDetails

data class PokemonDetailsState(
    val pokemon: PokemonDetails? = null,
    val isIdle: Boolean = false,
    val isLoading: Boolean = false,
    val isError: Boolean = false,
    val total: Int = 0,
    val page: Int = 0
)