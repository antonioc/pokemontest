package com.wultimaproject.pokemontest.presentation.list

sealed class PokemonListAction {
    data class LoadPokemonsList(val forward:Boolean): PokemonListAction()
}