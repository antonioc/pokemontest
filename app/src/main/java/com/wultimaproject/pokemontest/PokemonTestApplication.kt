package com.wultimaproject.pokemontest

import android.app.Application
import com.wultimaproject.pokemontest.di.networkModule
import com.wultimaproject.pokemontest.di.viewModelModule
import com.wultimaproject.pokemontest.repository.pokemonListRepository
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class PokemonTestApplication : Application() {
    override fun onCreate() {
        super.onCreate()

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        startKoin {
            androidContext(this@PokemonTestApplication)
            modules(listOf(viewModelModule, networkModule,
                pokemonListRepository
            ))
        }

    }
}