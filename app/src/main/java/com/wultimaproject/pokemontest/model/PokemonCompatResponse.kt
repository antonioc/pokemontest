package com.wultimaproject.pokemontest.model

data class PokemonCompatResponse(
    val count : Int,
    val results : List<PokemonCompat>
)