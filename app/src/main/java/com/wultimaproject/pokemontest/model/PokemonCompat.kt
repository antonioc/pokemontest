package com.wultimaproject.pokemontest.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PokemonCompat(
    val name: String,
    val url: String
): Parcelable