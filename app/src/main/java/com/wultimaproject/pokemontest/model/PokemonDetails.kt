package com.wultimaproject.pokemontest.model

data class PokemonDetails(

    val namePokemon : String,
    val sprites: String,
    val stats: List<Stats>,
    val types: List<String>
)

data class Stats(
    val baseStateName: String,
    val baseStateValue : Int
)