package com.wultimaproject.pokemontest.model

data class PokemonDetailsResponse(
    val name: String,
    val sprites: Sprites,
    val stats: List<GroupStatsSchemas>,
val types: List<GroupTypes>
)


data class Sprites(
    val front_shiny: String
)


data class GroupStatsSchemas(
    val stat:StatSchemas,
    val base_stat: Int,
    val effort: Int
)

data class GroupTypes(
    val slot: Int,
    val type: Type
)

data class StatSchemas(
    val name: String,
    val url: String
)

data class Type(
    val name: String
)