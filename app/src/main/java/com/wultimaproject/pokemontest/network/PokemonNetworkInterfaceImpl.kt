package com.wultimaproject.pokemontest.network

import com.wultimaproject.pokemontest.model.PokemonCompatResponse
import com.wultimaproject.pokemontest.model.PokemonDetailsResponse
import io.reactivex.Observable
import org.koin.java.KoinJavaComponent.inject

class PokemonNetworkInterfaceImpl : PokemonNetworkInterface {

    private val networkClient: NetworkClient by inject(NetworkClient::class.java)

    override fun loadPokemonList(offset: Int): Observable<PokemonCompatResponse> {
        return networkClient.getPokemonsList(offset, 20)
    }

    override fun loadPokemonDetails(url: String): Observable<PokemonDetailsResponse> {
        return networkClient.getPokemonDetails(url)
    }
}