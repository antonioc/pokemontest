package com.wultimaproject.pokemontest.network

import com.wultimaproject.pokemontest.model.PokemonCompatResponse
import com.wultimaproject.pokemontest.model.PokemonDetailsResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface NetworkClient {
    @GET("pokemon")
    fun getPokemonsList(
        @Query("offset") offset: Int,
        @Query("limit") limitResults: Int
    ): Observable<PokemonCompatResponse>


    @GET
    fun getPokemonDetails(@Url url: String): Observable<PokemonDetailsResponse>
}