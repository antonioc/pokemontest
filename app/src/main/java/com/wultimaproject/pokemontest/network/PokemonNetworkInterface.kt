package com.wultimaproject.pokemontest.network

import com.wultimaproject.pokemontest.model.PokemonCompatResponse
import com.wultimaproject.pokemontest.model.PokemonDetailsResponse
import io.reactivex.Observable

interface PokemonNetworkInterface {
    fun loadPokemonList(offset: Int): Observable<PokemonCompatResponse>
    fun loadPokemonDetails(url: String): Observable<PokemonDetailsResponse>
}