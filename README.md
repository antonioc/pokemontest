# Pokemon Test

> A simple Pokédex with MVI-RxAndroid





## Tech

- Kotlin
- MVI
- Retrofit
- RxAndroid
- LiveData
- Koin


**minSDK: 24**


**Tested on**
Android Studio Emulator, Motorola G6, Xiaomi Mi A3